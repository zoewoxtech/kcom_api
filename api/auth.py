from functools import wraps

from flask import g, request, jsonify
from flask_login import current_user

from .models import Role
from .helper import kcom_errors


def role_validation(roles):
    def validation(f):
        @wraps(f)
        def decorated(*args,**kwargs):
            if not isinstance(roles, list) or len(roles) <= 0:
                return kcom_errors("Invalid role", 400)

            for role in roles:
                valid_role = Role.query.filter_by(title=role).first()

                if not valid_role:
                    return kcom_errors("Invalid role", 400)

                if role == str(current_user.role.title):
                    return f(*args, **kwargs)
                    
            return kcom_errors("You are not authorized to perform this operation", 401)
        
        return decorated

    return validation

def validate_type(item, input_type):
    return type(item) is input_type

def validate_request(*expected_args):
    """ This method validates the Request payload.
    Args
        expected_args(tuple): where i = 0 is type and i > 0 is argument to be
                            validated
    Returns
      f(*args, **kwargs)
    """

    def real_validate_request(f):
        type_map = {"str": "string",
                    "list": "list",
                    "dict": "dictionary",
                    "int": "integer"}

        @wraps(f)
        def decorated(*args,**kwargs):
            if not request.json:
                return {"status": "fail",
                        "data": {"message": "Request must be a valid JSON"}
                       }, 400
            payload = request.get_json()
            if payload:
                for values in expected_args:
                    for value in values:
                        if value == values[0]:
                            continue
                        
                        if value not in payload or ((values[0] != dict and not payload[value])):
                            return kcom_errors(value.replace('_', ' ') + " is required", 400)
                        elif not validate_type(payload[value], values[0]):
                            return kcom_errors(value.replace('_', ' ') + " must be a/an " + type_map[values[0].__name__], 400)
                        elif (values[0] == str and not payload[value].strip(' ')):
                            return kcom_errors(value.replace('_', ' ') + " must be a valid string", 400)

            return f(*args, **kwargs)

        return decorated

    return real_validate_request
