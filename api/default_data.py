import os

from datetime import datetime

from .models import (User, Role, NodeType)

def create_default_roles():
    roles = ['super_admin', 'admin', 'authenticated', 'guest']
    for role in roles:
        role = Role(
            title=role,
            description='{} privilege'.format(role)
        )
        role.save()

def create_default_user(role_id):
    super_user = User(
            title="Mr.",
            firstname="default",
            lastname="default",
            middlename="default",
            username=os.environ.get('SUPER_ADMIN_USERNAME'),
            email=os.environ.get('SUPER_ADMIN_EMAIL'),
            password=os.environ.get('SUPER_ADMIN_PASSWORD'),
            role_id=role_id,
            bio="default",
            dob=datetime.utcnow()
        )
    super_user.save()

def create_default_node_types(user_id):
    node_types = ['blog', 'daily_devotional', 'page']
    for node_type in node_types:
        node_type = NodeType(
                name=node_type,
                user_id=user_id
            )
        node_type.save()
