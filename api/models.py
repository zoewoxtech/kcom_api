import os
import json

from flask import abort
from flask_login import UserMixin
from flask_sqlalchemy import BaseQuery, SQLAlchemy
from sqlalchemy.orm import backref
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.types import JSON, TEXT, TypeDecorator
from datetime import datetime
from sqlalchemy import event
from werkzeug import generate_password_hash, check_password_hash, exceptions

from .id_generator import PushID

class GetOrQuery(BaseQuery):
    def get_or_404(self, ident):
        rv = self.get(ident)
        if rv is None:
            e = exceptions.NotFound('Resource not found')
            e.data = {
                'status': 'fail',
                "data": {
                    "message": "This resource does not exist"
                }
            }
            raise e
        return rv


class StringyJSON(TypeDecorator):
    """Stores and retrieves JSON as TEXT."""

    impl = TEXT

    def process_bind_param(self, value, dialect):
        """Map value into json data."""
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        """Map json data to python dictionary."""
        if value is not None:
            value = json.loads(value)
        return value


# TypeEngine.with_variant says "use StringyJSON instead when
# connecting to 'sqlite'"
MagicJSON = JSON().with_variant(StringyJSON, 'sqlite')

type_map = {'sqlite': MagicJSON, 'postgresql': JSON}
json_type = type_map[os.getenv("DB_TYPE")]

db = SQLAlchemy(query_class=GetOrQuery)

class ModelViewsMix(object):
    def save(self):
        """Saves an instance of the model to the database."""
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except SQLAlchemyError as error:
            db.session.rollback()
            return error
    
    def delete(self):
        """Delete an instance of the model from the database."""
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except SQLAlchemyError as error:
            db.session.rollback()
            return error


# many to many relationship between contributions and tags
node_tag = db.Table('node_tag',
    db.Column('tag_id', db.String, db.ForeignKey('Tag.id'), nullable=False),
    db.Column('node_id', db.Integer, db.ForeignKey('Node.id'), nullable=False)
)


class NodeType(db.Model, ModelViewsMix):

    __tablename__ = 'NodeType'
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    node_type = db.relationship('Node', backref='node_type', lazy='dynamic')
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    user_id = db.Column('user_id', db.String, db.ForeignKey('User.id'))

    def __repr__(self):
        return '<NodeType %r>' % self.name


class Node(db.Model, ModelViewsMix):

    __tablename__ = 'Node'

    id = db.Column(db.Integer, primary_key=True)
    node_type_id = db.Column('node_type_id', db.Integer, db.ForeignKey('NodeType.id'))
    title = db.Column(db.String(120), nullable=False)
    content = db.Column(db.Text, nullable=False)
    slug = db.Column(db.String(20), unique=True)
    publish_date = db.Column(db.DateTime, nullable=True)
    status = db.Column(db.String(10), default='draft')
    user_id = db.Column(
        'user_id', db.String, db.ForeignKey('User.id'))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    modified_at = db.Column(db.DateTime, default=datetime.utcnow,
            onupdate=datetime.utcnow)
    meta = db.Column(json_type)

    def __repr__(self):
        return '<Node %r>' % self.title


class Tag(db.Model, ModelViewsMix):

    __tablename__ = 'Tag'

    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String(20), unique=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Tag %r>' % self.name


class Role(db.Model, ModelViewsMix):

    __tablename__ = 'Role'

    id = db.Column(db.String, primary_key=True)
    title = db.Column(db.String, unique=True)
    description = db.Column(db.String, nullable=True)
    users = db.relationship('User', backref='role', lazy='dynamic')
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    modified_at = db.Column(db.DateTime, default=datetime.utcnow,
            onupdate=datetime.utcnow)


class User(UserMixin, db.Model, ModelViewsMix):

    __tablename__ = 'User'
    
    id = db.Column(db.String, primary_key=True)
    title = db.Column(db.String(15))
    firstname = db.Column(db.String(30), nullable=False)
    middlename = db.Column(db.String(30))
    lastname = db.Column(db.String(30), nullable=False)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(50), unique=True, nullable=False)
    dob = db.Column(db.DateTime)
    _password = db.Column('password', db.String(), nullable=False)
    status= db.Column(db.String(), default='blocked')
    bio = db.Column(db.Text)
    role_id = db.Column(db.String(), db.ForeignKey('Role.id'))
    node_author = db.relationship(
        'Node', backref='author', cascade="all, delete-orphan",
        lazy='dynamic')
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    modified_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
      return '<User %r>' % (self.username)

    def _get_password(self):
      return self._password

    def _set_password(self, password):
      self._password = generate_password_hash(password)

    password = db.synonym('_password',
                          descriptor=property(_get_password,
                                              _set_password))

    def check_password(self, password):
      if self.password is None:
         return False
      return check_password_hash(self.password, password)

    #   class methods
    @classmethod
    def authenticate(cls, username, password):
        user = User.query.filter(db.or_(User.username == username)).first()

        if user:
            authenticated = user.check_password(password)
        else:
            authenticated = False
        return user, authenticated

    @classmethod
    def is_user_data_taken(cls, username, email):
       return db.session.query(db.exists().where(User.email==email or User.username==username)).scalar()


def fancy_id_generator(mapper, connection, target):
    '''
    A function to generate unique identifiers on insert
    '''
    push_id = PushID()
    target.id = push_id.next_id()

# associate the listener function with models, to execute during the
# "before_insert" event
tables = [Tag, User, Role]

for table in tables:
    event.listen(table, 'before_insert', fancy_id_generator)
