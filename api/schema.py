from marshmallow import Schema, fields, validate, pre_load, post_dump, validates_schema, ValidationError
from datetime import datetime as dt


def check_unknown_fields(data, original_data, fields):
    unknown = set(original_data) - set(fields)
    if unknown:
        raise ValidationError('{} is not a valid field'.format(), unknown)

class TagSchema(Schema):
    id = fields.Str(dump_only=True)
    name = fields.Str(
        required=True,
        errors={
            'required': 'Please provide a tag name.',
            'type': 'Invalid type'
        }
    )
    created_at = fields.DateTime(dump_only=True)


class LoginSchema(Schema):
    id = fields.Str(dump_only=True)
    userId = fields.Str(
        required=True,
        errors={
            'required': 'Please provide a valid username or email.',
            'type': 'Invalid type'
        })
    password = fields.Str(
        required=True,
        errors={
            'required': 'Please provide your password.',
            'type': 'Invalid type'
        })

    @validates_schema(pass_original=True)
    def unknown_fields(self, data, original_data):
        check_unknown_fields(data, original_data, self.fields)


class RoleSchema(Schema):
    id = fields.Str(dump_only=True)
    title = fields.Str(
        required=True,
        errors={
            'required': 'Please provide a title.',
            'type': 'Invalid type'
        })
    description = fields.Str(
        required=True,
        errors={
            'required': 'Please provide a description.',
            'type': 'Invalid type'
        })
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)


class UserSchema(Schema):
    id = fields.Str(dump_only=True)
    title = fields.Str()
    firstname = fields.Str(
        required=True,
        errors={
            'required': 'Please provide your firstname.',
            'type': 'Invalid type'
        })
    middlename = fields.Str(errors={'type': 'Invalid type'})
    lastname = fields.Str(
        required=True,
        errors={
            'required': 'Please provide your lastname.',
            'type': 'Invalid type'
        })
    username = fields.Str(
        required=True,
        unique=True,
        errors={
            'required': 'Please choose a username.',
            'type': 'Invalid type'
        })
    email = fields.Str(
        required=True,
        errors={
            'required': 'Please provide a valid email.',
            'type': 'Invalid type'
        })
    dob = fields.DateTime('%Y-%m-%d', errors={'type': 'Invalid type'})
    bio = fields.Str(errors={'type': 'Invalid type'})
    role_id = fields.Str()
    password = fields.Str(load_only=True)
    status = fields.Str(errors={'type': 'Invalid type'})
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)

    @validates_schema(pass_original=True)
    def unknown_fields(self, data, original_data):
        check_unknown_fields(data, original_data, self.fields)


class NodeTypeSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(
        required=True,
        unique=True,
        error_messages={
            'required': 'The node-type name is required',
            'invalid': 'Invalid node-type name provided'
        })
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)


class NodeSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str(
        required=True,
        error_messages={
            'required': 'The title is required',
            'invalid': 'Invalid title provided'
        })
    node_type_id = fields.Int()
    content = fields.Str(
        required=True,
        error_messages={
            'required': 'Content cannot be empty.',
            'invalid': 'Invalid content provided.'
        })
    slug = fields.Str(
        validate=[validate.Length(max=20, error='Slug cannot be more than 20 characters long')],
        error_messages={
            'invalid': 'Invalid slug provided.'
        })
    publish_date = fields.Date(error_messages={'type': 'Invalid type'})
    user_id = fields.Str()
    status = fields.Str()
    meta = fields.Dict()
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)

    @validates_schema(pass_original=True)
    def unknown_fields(self, data, original_data):
        check_unknown_fields(data, original_data, self.fields)


role_schema = RoleSchema()
tag_schema = TagSchema()
user_schema = UserSchema()
login_schema = LoginSchema()
node_schema = NodeSchema()
node_type_schema = NodeTypeSchema()
