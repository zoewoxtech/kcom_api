import os
import nltk
import string
from sqlalchemy import desc, asc
nltk.download('stopwords')
nltk.download('punkt')

from datetime import datetime
from hashids import Hashids
from nltk.corpus import names, stopwords, words
from nltk.tokenize import word_tokenize

from .models import Node, User

def kcom_errors(errors, status_code):
    return {
        'status': 'fail',
        'data': { 'message': errors }
    }, status_code

def validate_input_data(data, keys, _status=None, resource=None):
    if not set(list(data.keys())) <= set(keys):
        return kcom_errors('No invalid field(s) allowed', 400)
    elif data.get('status') and data.get('status') not in _status:
        return kcom_errors('Incorrect status selected', 400)

def validate_empty_fields(data):
    array = list(data.values())
    empty = [item for item in array if not item.strip()]
    if empty:
        return kcom_errors('Fields cannot be empty', 400)

def generate_unique_slug(title, node_type_id):
    # REMOVE ANY PUNCTUATION MARKS IN TITLE STRING
    table = title.maketrans({key: None for key in string.punctuation})
    title = title.translate(table)

    # GET LIST OF STOP WORDS
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(title)
    # REMOVE STOPWORDS
    filtered_sentence = [w for w in word_tokens if not w in stop_words]
    # CONVERT TITLE TO SLUG
    slug = '_'.join(filtered_sentence[:5]).lower()

    # ADD CHECK TO ENSURE THE SLUG IS UNIQUE
    _node = Node.query.filter(Node.slug.like('%{}%'.format(slug)), node_type_id==node_type_id).order_by(desc('slug')).first()

    if _node:
        if not _node.slug.rsplit('_', 1)[1].isdigit():
            slug = slug + '_1'
        else:
            slug_no = int(_node.slug.rsplit('_', 1)[1]) + 1
            slug = slug + '_' + str(slug_no)

    return slug

def check_user_role(user_role, roles_to_check):
    for role in roles_to_check:
        if user_role == role:
            return True
    return False

def create_default_user(super_role, admin_role, authenticated_role, guest_role):
    super_admin = User(
            title="Mr.", 
            firstname="super_admin",
            lastname="super_admin",
            middlename="middlename",
            username="super_admin",
            email="super_admin@mail.com",
            password="password",
            role_id=super_role,
            bio="bio",
            dob=datetime.utcnow()
        )
    super_admin.save()
    admin = User(
            title="Mr.",
            firstname="admin",
            lastname="admin",
            middlename="middlename",
            username="admin",
            email="admin@mail.com",
            password="password",
            role_id=admin_role,
            bio="bio",
            dob=datetime.utcnow()
        )
    admin.save()
    guest = User(
            title="Mr.",
            firstname="guest",
            lastname="guest",
            middlename="middlename",
            username="guest",
            email="guest@mail.com",
            password="password",
            role_id=guest_role,
            bio="bio",
            dob=datetime.utcnow()
        )
    guest.save()
    johndoe = User(
            title="Mr.",
            firstname="john",
            lastname="doe",
            middlename="middlename",
            username="johndoe",
            email="john@doe.com",
            password="password",
            role_id=authenticated_role,
            bio="bio",
            dob=datetime.utcnow()
        )
    johndoe.save()
    janedoe = User(
            title="Miss.",
            firstname="jane",
            lastname="doe",
            middlename="middlename",
            username="janedoe",
            email="jane@doe.com",
            password="password",
            role_id=authenticated_role,
            bio="bio",
            dob=datetime.utcnow()
        )
    janedoe.save()
