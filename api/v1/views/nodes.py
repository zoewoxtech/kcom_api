from flask import current_app, request, jsonify, json, Response, url_for
from flask_login import login_required, current_user
from flask_restful import Resource
from sqlalchemy import desc, asc

from ...auth import role_validation, validate_request
from ...helper import (
    kcom_errors, validate_input_data, check_user_role,
    generate_unique_slug
)
from ...models import Node, NodeType
from ...schema import node_schema


class NodeResource(Resource):
    
    def get(self, node_type, node_id):
        _node_type = NodeType.query.filter_by(name=node_type).first()
        if not _node_type:
            return kcom_errors('The requested content-type does not exist', 404)

        if node_id.isdigit():
            _node = Node.query.filter_by(id=node_id, node_type_id=_node_type.id).first()
        elif isinstance(node_id, str):
            _node = Node.query.filter_by(slug=node_id, node_type_id=_node_type.id).first()
        else:
            return kcom_errors('An error occured, the requested {} could not be found.'.format(_node_type), 400)

        if _node:
            if _node.status == 'draft' and not current_user.is_authenticated:
                return kcom_errors('You are not authorized to view this content', 400)
            
            if _node.status == 'draft' and current_user.is_authenticated and \
            not check_user_role(current_user.role.title, ['super_admin', 'admin']) \
            and current_user.id != _node.user_id:
                return kcom_errors('You are not authorized to view this content', 400)

            _data, errors = node_schema.dump(_node)
            if errors:
                return kcom_errors(errors, 422)

            return {
                'status': 'success',
                'data': { 'node': _data }
            }, 200

        return kcom_errors('This content does not exist', 404)

    @login_required
    @role_validation(["super_admin", "admin", "authenticated"])
    @validate_request()
    def put(self, node_type, node_id):
        json_input = request.get_json()
        _node_type = NodeType.query.filter_by(name=node_type).first()
        if not _node_type:
            return kcom_errors("The requested content-type does not exist", 404)
        if isinstance(node_id, str):
            _node = Node.query.filter_by(slug=node_id, node_type_id=_node_type.id).first()
        elif isinstance(node_id, int):
            _node = Node.query.filter_by(id=node_id, node_type_id=_node_type.id).first()
        else:
            return kcom_errors('An error occured, the requested {} could not be found.'.format(_node_type), 400)
        if not _node:
            return kcom_errors("The requested {0} does not exist".format(_node_type.name), 404)

        if not check_user_role(current_user.role.title, ['super_admin', 'admin']) and \
        node_type is not 'blog' and current_user.id != _node.user_id:
            return kcom_errors("You are not authorized to update this {0}".format(node_type), 401)

        if json_input.get("slug"):
            _node_slug_exists = Node.query.filter_by(slug=json_input["slug"]).first()
            if _node_slug_exists and _node_slug_exists.id is not int(_node.id):
                return kcom_errors("{0} with this title already exists".format(node_type), 400)

        keys = ["slug", "title", "content", "meta", "status", "publish_date"]
        _status = ["draft", "published"]

        if validate_input_data(json_input, keys, _status):
            return validate_input_data(json_input, keys, _status)
        else:
            [setattr(_node, key, json_input[key]) for key in json_input.keys()]
        _node.save()

        updated_node = node_schema.dump(_node).data
        message = "Successfully updated post with title {}".format(updated_node["title"])
        return {
            "status": "success",
            "data": {
                "node": updated_node,
                "message": message
            }
        }, 200


class NodeListResource(Resource):
    
    def get(self, node_type):
        page = request.args.get('page', current_app.config['DEFAULT_PAGE'], type=int)
        limit = request.args.get('limit', current_app.config['PAGE_LIMIT'], type=int)
        _search = request.args.get('search', None)
        _sort = request.args.get('sort', 'publish_date')
        _order = request.args.get('order', 'desc')
        _nodes = []
  
        if node_type.lower() in ['page', 'pages', 'static_page', 'static_pages']:
            return kcom_errors("This url doesn't exist", 404)

        _node_type = NodeType.query.filter_by(name=node_type).first()
        if not _node_type:
            return kcom_errors('The {} node does not exist'.format(node_type), 400)

        nodes = (
                    Node.query.filter_by(node_type_id=_node_type.id,
                    status="published").order_by(asc(_sort)) 
                    if _order and _order == 'asc'
                    else Node.query.filter_by(node_type_id=_node_type.id,
                    status="published").order_by(desc(_sort))
                )
        message = 'Found {} {}s'.format(nodes.count(), node_type.replace('_', ' '))
        if _search:
            nodes = nodes.filter(
                Node.title.ilike('%{}%'.format(_search)))
            message = 'Found {} {}s matching search criteria'.format(nodes.count(), node_type.replace('_', ' '))

        _nodes_paginated = nodes.paginate(page=page, per_page=limit, error_out=True)

        for node in _nodes_paginated.items:
            data, errors = node_schema.dump(node)
            if errors:
                return kcom_errors(errors, 400)
            _nodes.append(data)

        prev_url = None
        next_url = None

        if _nodes_paginated.has_next:
            next_url = url_for(request.endpoint,
                        node_type=node_type,
                        limit=limit,
                        page=_nodes_paginated.next_num,
                        _external=True)
        if _nodes_paginated.has_prev:
            prev_url = url_for(request.endpoint,
                        node_type=node_type,
                        limit=limit,
                        page=_nodes_paginated.prev_num,
                        _external=True)

        return {
            'status': 'success',
            'data': {
                'current_count': len(_nodes),
                'current_page': _nodes_paginated.page,
                'message': message,
                'next_url': next_url,
                'nodes': _nodes,
                'prev_url': prev_url,
                'total_nodes': nodes.count(),
                'total_pages': _nodes_paginated.pages
            }
        }, 200


    @login_required
    @role_validation(["super_admin", "admin"])
    def patch(self, node_type):
        node_was_deleted = False
        node_wasnt_deleted = False
        node_not_found = False
        invalid_node_id = False
        _message = ''

        _node_type = NodeType.query.filter_by(name=node_type).first()
        if not _node_type:
            return kcom_errors("The requested content-type doesn't exist", 404)
        req_data = request.get_json()

        if len(req_data['nodes']) <= 0:
            return kcom_errors('Action failed. No posts selected.', 400)

        for _node_id in req_data['nodes']:
            if isinstance(_node_id, int):
                _node = Node.query.filter_by(id=_node_id, node_type_id=_node_type.id).first()
            elif isinstance(_node_id, str):
                _node = Node.query.filter_by(slug=_node_id, node_type_id=_node_type.id).first()
            else:
                invalid_node_id = True

            if _node:
                # check if deleter is admin/super_admin or the owner
                if not check_user_role(current_user.role.title, ['super_admin', 'admin']) and \
                node_type is not 'blog' and current_user.id != _node.user_id:
                    return kcom_errors(f"You are not authorized to delete this {node_type}", 401)
                else:
                    if req_data['nodeAction'] == 'trash' and _node.status != 'trash':
                        _node.status = 'trash'
                        _node.save()
                        node_was_deleted = True
                    elif req_data['nodeAction'] == 'delete' and _node.status == 'trash':
                        _node.delete()
                        node_was_deleted = True
                    else:
                        node_wasnt_deleted = True
            else:
                node_not_found = True

        if node_was_deleted:
            _message += 'Post(s) successfully trashed. '
        if node_wasnt_deleted:
            _message += 'Some post(s) could not trashed. '
        if node_not_found or invalid_node_id:
            _message += 'Some post(s) could not be found. '
 
        return {
            'status': 'success',
            'data': {
                'message': _message
            }
        }, 200


class NodeAccessListResource(Resource):
    
    @login_required
    def get(self, node_type):
        page = request.args.get('page', current_app.config['DEFAULT_PAGE'], type=int)
        limit = request.args.get('limit', current_app.config['PAGE_LIMIT'], type=int)
        _status = request.args.get('status', None)
        _search = request.args.get('search', None)
        _sort = request.args.get('sort', 'publish_date')
        _order = request.args.get('order', 'desc')
        _nodes = []

        _node_type = NodeType.query.filter_by(name=node_type.lower()).first()
        if not _node_type:
            return kcom_errors('The {} content type does not exist'.format(node_type), 400)

        nodes = Node.query.filter_by(node_type_id=_node_type.id)
        
        if not check_user_role(current_user.role.title, ['super_admin', 'admin']):
            if node_type.lower() in ['page', 'pages', 'static_page', 'static_pages']:
                return kcom_errors('You are not authorized to view this node', 400)

            nodes = Node.query.filter(
                    (Node.node_type_id==_node_type.id) & 
                    (Node.status=="published") | 
                    (Node.user_id==current_user.id)
                ).order_by('{} {}'.format(_sort, _order))
        if _search:
            nodes = nodes.filter(
                Node.title.ilike('%{}%'.format(_search)))
            message = 'Found {} {} matching search criteria'.format('', node_type)

        if _status:
            nodes = nodes.filter(
                Node.status.ilike('%{}%'.format(_status)))

        _nodes_paginated = nodes.paginate(page=page, per_page=limit, error_out=True)

        for node in _nodes_paginated.items:
            data, errors = node_schema.dump(node)
            if errors:
                return kcom_errors(errors, 400)
            _nodes.append(data)

        prev_url = None
        next_url = None

        if _nodes_paginated.has_next:
            next_url = url_for(request.endpoint,
                        node_type=node_type,
                        limit=limit,
                        page= _nodes_paginated.next_num,
                        _external=True)
        if _nodes_paginated.has_prev:
            prev_url = url_for(request.endpoint,
                        node_type=node_type,
                        limit=limit,
                        page= _nodes_paginated.prev_num,
                        _external=True)

        return {
            'status': 'success',
            'data': {
                'current_count': len(_nodes),
                'current_page': _nodes_paginated.page,
                'next_url': next_url,
                'nodes': _nodes,
                'prev_url': prev_url,
                'total_nodes': nodes.count(),
                'total_pages': _nodes_paginated.pages
            }
        }, 200


    @login_required
    @role_validation(["super_admin", "admin"])
    @validate_request((str, "title", "content"), (dict, "meta"))
    def post(self, node_type):
        json_input = request.get_json()

        # check if selected node_type exists
        _node_type = NodeType.query.filter_by(name=node_type).first()
        if not _node_type:
            return kcom_errors('The requested content type does not exist', 404)

        if not check_user_role(current_user.role.title, ['super_admin', 'admin']) and node_type != 'blog':
            return kcom_errors("You are not authorized to create a {0}".format(node_type), 401)

        _slug = json_input.get('slug') or generate_unique_slug(json_input['title'], _node_type.id)
        
        _node = Node.query.filter_by(slug=_slug, node_type_id=_node_type.id).first() or {}

        if _node:
            return kcom_errors("{} already exists with title '{}'".format(_node_type.name, json_input['title']), 400)

        keys = ['title', 'content', 'meta', 'status', 'publish_date']
        _status = ['draft', 'published']

        if not json_input.get('status'):
            json_input['status'] = 'draft'

        if validate_input_data(json_input, keys, _status, _node):
            return validate_input_data(json_input, keys, _status, _node)

        data, errors = node_schema.load(json_input, partial=('publish_date'))
        if errors:
            return kcom_errors(errors, 422)
        _publish_date = json_input.get('publish_date') or '1000-01-01'

        new_node = Node(
            title=data['title'],
            content=data['content'],
            slug=_slug,
            node_type_id=_node_type.id,
            status=data['status'],
            user_id=current_user.id,
            publish_date=_publish_date,
            meta=data['meta']
        )
        new_node.save()

        _data, _ = node_schema.dump(new_node)
        message = "Successfully created post with title {}".format(new_node.title)

        return {
            'status': 'success',
            'data': {
                'node': _data,
                'message': message
            }
        }, 201

