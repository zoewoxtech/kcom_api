import datetime
import copy

from flask import current_app, request, jsonify, json, Response, url_for
from flask_restful import Resource, reqparse
from flask_jwt import jwt
from flask_login import login_user, logout_user, login_required, current_user
from sqlalchemy import or_

from ...helper import kcom_errors, validate_input_data, validate_empty_fields
from ...auth import role_validation, validate_request
from ...models import (db, User, Role)
from ...schema import (user_schema, login_schema)

class UserResource(Resource):
    @login_required
    def get(self, user_id):
        _user = User.query.get_or_404(user_id)
        _data, errors = user_schema.dump(_user)
        if errors:
            return kcom_errors(errors, 422)
        _data['role'] = _user.role.title
        return {
            'status': 'success',
            'data': { 'user': _data }
        }, 200

    @login_required
    @validate_request()
    def put(self, user_id):
        json_input = request.get_json()
        _json_input = copy.deepcopy(json_input)
        keys = ['title', 'firstname', 'lastname', 'middlename', 'bio', 'password', 'dob']
        empty_fields = ['middlename', 'bio']

        if current_user.id == user_id or current_user.role.title in ['super_admin', 'admin']:
            if current_user.role.title == 'super_admin' and current_user.id == user_id and json_input.get('role_id'):
                return kcom_errors('You are not allowed to update your role', 400)
            if current_user.role.title == 'super_admin':
                new_keys = ['username', 'email', 'role_id']
                [keys.append(key) for key in new_keys]
            
            _user = User.query.get(user_id)

            if validate_input_data(json_input, keys, _user):
                return validate_input_data(json_input, keys, _user)
            for item in empty_fields:
                _json_input.pop(item, None)
            if validate_empty_fields(_json_input):
                return validate_empty_fields(_json_input)

            [setattr(_user, key, json_input[key]) for key in json_input.keys()]
            _user.save()
            updated_user = user_schema.dump(_user).data
            return {
                'status': 'success',
                'data': {
                    'user': updated_user,
                    'message': 'User profile updated successfully'
                }
            }, 200
        return kcom_errors('You are not permitted to perform this operation', 401)


class UserListResource(Resource):
    @login_required
    @role_validation(['super_admin', 'admin'])
    def get(self):
        page = request.args.get('page', current_app.config['DEFAULT_PAGE'], type=int)
        limit = request.args.get('limit', current_app.config['PAGE_LIMIT'], type=int)
        _limit = request.args.get('limit')
        _search = request.args.get('search', None)
        _sort = request.args.get('sort', 'created_at')
        _order = request.args.get('order', 'desc')
        _users = []

        users = User.query.order_by(User.created_at.desc())
        _users_paginated = users.paginate(page=page, per_page=limit, error_out=True)

        for user in _users_paginated.items:
            data, errors = user_schema.dump(user)
            data['role'] = user.role.title
            if errors: 
                return kcom_errors(errors, 400)
            _users.append(data)

        prev_url = None
        next_url = None

        if _users_paginated.has_next:
            next_url = url_for(request.endpoint,
                        limit=limit,
                        page=_users_paginated.next_num,
                        _external=True)
        if _users_paginated.has_prev:
            prev_url = url_for(request.endpoint,
                        limit=limit,
                        page=_users_paginated.prev_num,
                        _external=True)

        return {
            'status': 'success',
            'data': {
                'current_count': len(_users),
                'current_page': _users_paginated.page,
                'next_url': next_url,
                'users': _users,
                'prev_url': prev_url,
                'total_users': users.count(),
                'total_pages': _users_paginated.pages
            }
        }, 200


    @login_required
    @role_validation(['super_admin'])
    def delete(self):
        _users = request.get_json()['users']
        message = []

        for _user in _users:
            user = User.query.filter_by(username=_user).first()
            if not user:
                message.append('No user found with this username - {0}. '.format(_user))
            elif user.role.title in ['admin', 'super_admin']:
                message.append('The {0} user cannot be deleted. '.format(_user))
            elif user:
                user.delete()
                message.append('{0} User account deleted successfully. '.format(_user))

        return {
            'status': 'success',
            'data': {
                'message': message
            }
        }, 200


class UserSignupResource(Resource):
    @validate_request((str, 'firstname', 'lastname', 'username', 'email', 'password', 'confirmPassword', 'dob'))
    def post(self):
        json_input = request.get_json()

        if json_input.get('password') != json_input.get('confirmPassword'):
            return kcom_errors("The passwords entered don't match.", 400)

        if User.is_user_data_taken(json_input['username'], json_input['email']):
            return kcom_errors('User already exists', 400)
        else:
            _user = {}
        
        keys = ['title', 'firstname', 'lastname', 'middlename', 'bio', 'email', 'password', 'confirmPassword', 'username', 'role_id', 'dob', 'status']
        _status = ['blocked', 'active']

        if validate_input_data(json_input, keys, _status, _user):
            return validate_input_data(json_input, keys, _status, _user)

        if json_input.get('confirmPassword'):
            json_input.pop('confirmPassword')

        data, errors = user_schema.load(json_input)
        if errors:
            return kcom_errors(errors, 422)

        default_role = Role.query.filter_by(title='authenticated').first().id
        if json_input.get('role_id') and not Role.query.get(json_input['role_id']):
            return kcom_errors('Invalid role', 400)

        new_user = User(
            title=data['title'] if json_input.get('title') else '',
            firstname=data['firstname'],
            lastname=data['lastname'],
            middlename=data['middlename'] if json_input.get('middlename') else '',
            username=data['username'],
            email=data['email'],
            password=data['password'],
            role_id=json_input['role_id'] if json_input.get('role_id') else default_role,
            bio=data['bio'] if json_input.get('bio') else '',
            dob=data['dob']
        )
        new_user.save()

        message = "{0}'s profile successfully created".format(new_user.username)

        _data, _ = user_schema.dump(new_user)

        return {
            'status': 'success',
            'data': {
                'user': _data,
                'message': message
            }
        }, 201

class UserLoginResource(Resource):
    @validate_request((str, 'userId', 'password'))
    def post(self):
        json_input = request.get_json()

        data, errors = login_schema.load(json_input)
        if errors:
            return kcom_errors(errors, 422)

        keys = ['userId', 'password']
        if validate_input_data(json_input, keys):
            return validate_input_data(json_input, keys)
        
        _user = User.query.filter(or_(User.email.like(json_input['userId']),
                                        User.username.like(json_input['userId']))).first()
        if not _user:
            return kcom_errors('User does not exist', 404)

        if not _user.check_password(json_input['password']):
            return kcom_errors("Your password is incorrect, please try again", 401)

        exp_date = datetime.datetime.utcnow()
        payload = {
                    "UserInfo": {
                        "id":_user.id,
                        "email":_user.email,
                        "role": _user.role.title
                    },
                    "exp": exp_date + datetime.timedelta(days=3)
                }
        _token = jwt.encode(payload, 'safe-secret', algorithm='HS256').decode('utf-8')

        login_user(_user, remember=True)
        return jsonify({"status": "success",
                        "data": {
                            "message": "Login successful",
                            "token": str(_token)
                        }
                    })

class UserLogoutResource(Resource):
    def get(self):
        if request.headers.get('Authorization') or request.headers.get('Cookie'):
            logout_user()
            return {
                "status": "success",
                "data": {
                    "message": "You have been successfully logged out"
                }
            }, 200
        return kcom_errors("Only logged in users can log out", 400)
