from datetime import date, datetime
from flask import request, jsonify, json, Response
from flask_restful import Resource
from flask_login import login_required, current_user
from ...helper import kcom_errors, validate_input_data, validate_empty_fields

from ...auth import role_validation, validate_request
from ...models import Role
from ...schema import role_schema


class RoleResource(Resource):

    @login_required
    @role_validation(['super_admin'])
    @validate_request()
    def put(self, role_id):
        json_input = request.get_json()
        _role = Role.query.get_or_404(role_id)

        default_roles = ['super_admin', 'admin', 'authenticated', 'guest']
        if _role.title in default_roles:
            return kcom_errors('You cannot edit any of the default roles', 400)

        keys = ['title', 'description']
        if validate_input_data(json_input, keys):
            return validate_input_data(json_input, keys)
        if validate_empty_fields(json_input):
            return validate_empty_fields(json_input)

        if json_input.get('title'):
            existing_role = Role.query.filter_by(title=json_input.get('title')).first()
            if existing_role and json_input.get('title') == existing_role.title:
                return kcom_errors('Role with this title already exists', 400)

        [setattr(_role, key, json_input[key]) for key in json_input.keys()]
        _role.save()
        updated_role = role_schema.dump(_role).data
        return {
            'status': 'success',
            'data': { 'role': updated_role }
        }, 200

    @login_required
    @role_validation(["super_admin"])
    def delete(self, role_id):
        _role = Role.query.get_or_404(role_id)

        default_roles = ['super_admin', 'admin', 'authenticated', 'guest']
        if _role.title in default_roles:
            return kcom_errors('You cannot edit any of the default roles', 400)

        if not _role.delete():
            return kcom_errors('Role could not be deleted', 400)
        return {
            'status': 'success',
            'data': None
        }, 200


class RoleListResource(Resource):

    @login_required
    @role_validation(["super_admin"])
    def get(self):
        _page = request.args.get('page', 1)
        _limit = request.args.get('limit')
        _roles = []

        limit = _limit if type(_limit) is int and limit else 10
        roles = Role.query.order_by(Role.created_at.desc()).limit(limit)
        for role in roles:
            data, errors = role_schema.dump(role)
            if errors:
                return kcom_errors(errors, 400)
            _roles.append(data)

        return {
            'status': 'success',
            'data': { 'roles': _roles }
        }, 200

    @login_required
    @role_validation(["super_admin"])
    @validate_request((str, 'title', 'description'))
    def post(self):
        json_input = request.get_json()

        data, errors = role_schema.load(json_input)
        if errors:
            return kcom_errors(errors, 422)

        keys = ['title', 'description']
        if validate_input_data(json_input, keys):
            return validate_input_data(json_input, keys)

        if Role.query.filter_by(title=data['title']).first():
            return kcom_errors('Role already exists', 400)
          
        new_role = Role(
            title=data['title'],
            description=data['description']
        )
        new_role.save()

        message = "{0} role has been successfully created".format(new_role.title)

        _data, _ = role_schema.dump(new_role)

        return {
            'status': 'success',
            'data': {
                'role': _data,
                'message': message
            }
        }, 201
