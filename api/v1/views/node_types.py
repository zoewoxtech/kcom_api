from flask import current_app, request, jsonify, json, Response, url_for
from flask_restful import Resource
from flask_login import login_required, current_user
from ...helper import kcom_errors, validate_input_data

from ...models import Node, NodeType
from ...schema import node_type_schema
from ...auth import role_validation


class NodeTypeResource(Resource):

    @login_required
    @role_validation(["super_admin"])
    def get(self, node_type_id):
        _node_type = NodeType.query.get_or_404(node_type_id)
        _data, errors = node_type_schema.dump(_node_type)
        if errors:
            return kcom_errors(errors, 422)
        return {
            'status': 'success',
            'data': { 'node_type': _data }
        }, 200


    @login_required
    @role_validation(["super_admin"])
    def delete(self, node_type_id):
        _node_type = NodeType.query.get_or_404(node_type_id)
        _has_nodes = Node.query.filter_by(node_type_id=node_type_id).first()
        if _has_nodes:
            return kcom_errors('This content type has data and cannot be deleted', 400)
        _data, _ = node_type_schema.dump(_node_type)

        defaults = ['blog', 'daily devotional', 'page']

        if _data['name'].lower() in defaults:
            return kcom_errors('You cannot delete a default content type', 401)

        _node_type.delete()
        return {
            'status': 'success',
            'data': None
        }, 200


    @login_required
    @role_validation(["super_admin"])
    def put(self, node_type_id):
        if not request.json:
            return kcom_errors('Request must be a valid JSON', 400)

        json_input = request.get_json()

        _node_type = NodeType.query.get_or_404(node_type_id)

        default_types = ['blog', 'daily_devotional', 'page']

        if json_input.get('name') in default_types:
            return kcom_errors('You cannot edit any of the default content types', 401)
        _node_type_exists = NodeType.query.filter_by(name=json_input['name']).first()
        if _node_type_exists and _node_type_exists.id is not int(node_type_id):
            return kcom_errors('This content type already exists.', 400)

        if not json_input.get('name'):
            return kcom_errors('The content type name is required', 400)

        keys = ['name']

        if validate_input_data(json_input, keys):
            validate_input_data(json_input, keys)
        else:
            [setattr(_node_type, key, json_input[key]) for key in json_input.keys()]

        _node_type.save()
        updated_node_type = node_type_schema.dump(_node_type).data
        return {
            'status': 'success',
            'data': { 'node_type': updated_node_type }
        }, 200



class NodeTypeListResource(Resource):

    @login_required
    @role_validation(["super_admin"])
    def post(self):
        if not request.json:
            return kcom_errors('Request must be a valid JSON', 400)

        json_input = request.get_json()

        if not json_input.get('name'):
            return kcom_errors('The content type name is required', 400)

        _node_type = NodeType.query.filter_by(name=json_input['name'].lower()).first() or {}
        if _node_type:
            return kcom_errors('This content type already exists.', 400)

        keys = ['name']

        if validate_input_data(json_input, keys):
            validate_input_data(json_input, keys)
        
        data, errors = node_type_schema.load(json_input)
        if errors:
            return kcom_errors(errors, 422)

        new_node_type = NodeType(
            name=data['name'],
            user_id=current_user.id
        )
        new_node_type.save()
        message = "Successfully created {} content type".format(new_node_type.name)

        _data, _ = node_type_schema.dump(new_node_type)
        return {
            'status': 'success',
            'data': {
                'node_type': _data,
                'message': message
            }
        }, 201


    @login_required
    @role_validation(["super_admin"])
    def get(self):
        page = request.args.get('page', current_app.config['DEFAULT_PAGE'], type=int)
        limit = request.args.get('limit', current_app.config['PAGE_LIMIT'], type=int)
        _search = request.args.get('search', None)
        _sort = request.args.get('sort', 'created_at')
        _order = request.args.get('order', 'desc')
        _node_types = []

        node_types = NodeType.query.order_by(NodeType.created_at.desc())
        _node_types_paginated = node_types.paginate(page=page, per_page=limit, error_out=True)

        for node_type in node_types:
            data, errors = node_type_schema.dump(node_type)
            if errors: 
                return kcom_errors(errors, 400)
            _node_types.append(data)

        prev_url = None
        next_url = None

        if _node_types_paginated.has_next:
            next_url = url_for(request.endpoint,
                        limit=limit,
                        page=_node_types_paginated.next_num,
                        _external=True)
        if _node_types_paginated.has_prev:
            prev_url = url_for(request.endpoint,
                        limit=limit,
                        page=_node_types_paginated.prev_num,
                        _external=True)

        return {
            'status': 'success',
            'data': {
                'current_count': len(_node_types),
                'current_page': _node_types_paginated.page,
                'next_url': next_url,
                'node_types': _node_types,
                'prev_url': prev_url,
                'total_nodes': node_types.count(),
                'total_pages': _node_types_paginated.pages
            }
        }, 200
