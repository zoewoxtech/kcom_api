import os
from flask_jwt import jwt

from flask_script import Manager, Server, prompt_bool, Shell
from flask_migrate import MigrateCommand
from flask_login import LoginManager
from sqlalchemy.exc import SQLAlchemyError

from main import create_flask_app

try:
    from api.helper import (
        kcom_errors
    )
    from api.default_data import (
        create_default_roles, create_default_user, create_default_node_types
    )
    from api.models import db, Role, User, NodeType, Node, Tag, node_tag
except ModuleNotFoundError:
    from kcom_api.api.helper import (
        kcom_errors
    )
    from kcom_api.api.default_data import (
        create_default_roles, create_default_user, create_default_node_types
    )
    from kcom_api.api.models import db, Role, User, NodeType, Node, Tag, node_tag


environment = os.getenv("FLASK_CONFIG")
app = create_flask_app(environment)

app.secret_key = os.getenv("APP_SECRET")

login_manager = LoginManager()
login_manager.init_app(app)
# Set up user_loader
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

@login_manager.request_loader
def load_user_from_request(request):
    # next, try to login using Basic Auth
    api_key = request.headers.get('Authorization')
    if api_key:
        try:
            decoded = jwt.decode(api_key, 'safe-secret', algorithms=['HS256'])
            user_id = decoded['UserInfo']['id']
        except TypeError:
            return kcom_errors('Sorry, an error occured. Try again later', 400)
        user = User.query.get(user_id)
        if user:
            return user

    # finally, return None if both methods did not login the user
    return None

@login_manager.unauthorized_handler
def unauthorized_callback():
    return kcom_errors("You must be logged in to view this page", 401)

port = int(os.environ.get('PORT', 5000))
server = Server(host="0.0.0.0", port=port)

def _make_context():
    return dict(Node=Node, NodeType=NodeType, Tag=Tag, Role=Role, User=User, node_tag=node_tag)

# initialize flask script
manager = Manager(app)

# enable migration commands
manager.add_command("runserver", server)
manager.add_command("db", MigrateCommand)
manager.add_command("shell", Shell(make_context=_make_context))

@manager.command
def seed_default_data(prompt=True):
    if environment == "production":
        print("\n\n\tNot happening! Aborting...\n\n")
        return

    if environment in ["testing", "development"]:
        if (prompt_bool("\n\nAre you sure you want to seed your database, all previous data will be wiped off?")):
            try:
                # drops all the tables 
                db.drop_all()
                db.session.commit()

                # creates all the tables
                db.create_all()
            except SQLAlchemyError as error:
                db.session.rollback()
                print("\n\n\tRole table could not be deleted due to the error below! Aborting...\n\n\n" + str(error) + "\n\n")
                return

            try:
                # seed default roles
                create_default_roles()

                # seed default user
                role_id = Role.query.filter_by(title="super_admin").first().id
                create_default_user(role_id)

                # seed default node_types
                user_id = User.query.filter_by(username=os.environ.get("SUPER_ADMIN_USERNAME")).first().id
                create_default_node_types(user_id)
                message = "\n\n\tYay *\(^o^)/* \n\n Your database has been succesfully seeded !!! \n\n\t *\(@^_^@)/* <3 <3 \n\n"
            except SQLAlchemyError as error:
                db.session.rollback()
                message = "\n\n\tThe error below occured when trying to seed the database\n\n\n" + str(error) + "\n\n"

            print(message)

        else:
            print("\n\n\tAborting...\n\n")

    else:
        print("\n\n\tAborting... Invalid environment '{}'.\n\n"
              .format(environment))

manager.run()
