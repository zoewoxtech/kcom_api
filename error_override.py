errors = {
    'MethodNotAllowed': {
        'status': 405,
        'statusCode': 'fail',
        'message': 'This method is not allowed for the requested resource'
    },
    'ResourceDoesNotExist': {
        'status': 410,
        'statusCode': 'fail',
        'message': "A resource with that ID no longer exists."
    }
}
