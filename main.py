import os

from flask import Flask, jsonify
from flask_sslify import SSLify
from flask_cors import CORS
from flask_migrate import Migrate
from flask_restful import Api, abort


try:
    from config import app_configuration
    from error_override import errors
    from api.helper import kcom_errors
    from api.models import User
    from api.v1.views.users import (
        UserResource, UserListResource, UserSignupResource, UserLoginResource,
        UserLogoutResource
    )
    from api.v1.views.roles import RoleResource, RoleListResource
    from api.v1.views.node_types import NodeTypeResource, NodeTypeListResource
    from api.v1.views.nodes import (
        NodeListResource, NodeResource, NodeAccessListResource
    )
except ModuleNotFoundError:
    from kcom_api.config import app_configuration
    from kcom_api.error_override import errors
    from kcom_api.api.helper import kcom_errors
    from kcom_api.api.models import User
    from kcom_api.api.v1.views.users import (
        UserResource, UserListResource, UserSignupResource, UserLoginResource,
        UserLogoutResource
    )
    from kcom_api.api.v1.views.roles import RoleResource, RoleListResource
    from kcom_api.api.v1.views.node_types import NodeTypeResource, NodeTypeListResource
    from kcom_api.api.v1.views.nodes import (
        NodeListResource, NodeResource, NodeAccessListResource
    )

def create_flask_app(environment):
    app = Flask(__name__, instance_relative_config=True, static_folder=None)
    app.config.from_object(app_configuration[environment])
    app.config['BUNDLE_ERRORS'] = True

    try:
        from api import models
    except ModuleNotFoundError:
        from kcom_api.api import models

    # to allow cross origin resource sharing
    CORS(app)

    # initialize SQLAlchemy
    models.db.init_app(app)

    # initilize migration commands
    Migrate(app, models.db)

    # initilize api resources
    api = Api(app, errors=errors)

    environment = os.getenv("FLASK_CONFIG")

    # to redirect all incoming requests to https
    if environment.lower() == "production":
        sslify = SSLify(app, subdomains=True, permanent=True)

    # Landing page
    @app.route('/')
    def index():
        return "Welcome to the KCOM Api"

    ##
    ## Actually setup the Api resource routing here
    ##
    api.add_resource(NodeResource, '/api/v1/node/<node_type>/<node_id>', '/api/v1/node/<node_type>/<node_id>/', endpoint='node')
    api.add_resource(NodeListResource, '/api/v1/node/<node_type>', '/api/v1/node/<node_type>/', endpoint='nodes')
    api.add_resource(NodeAccessListResource, '/api/v1/node/access/<node_type>', '/api/v1/node/access/<node_type>/', endpoint='node_access')

    api.add_resource(NodeTypeListResource, '/api/v1/node_types', '/api/v1/node_types/', endpoint='node_types')
    api.add_resource(NodeTypeResource, '/api/v1/node_types/<node_type_id>', '/api/v1/node_types/<node_type_id>/', endpoint='single_node_type')

    api.add_resource(RoleResource, '/api/v1/roles/<role_id>', '/api/v1/roles/<role_id>/', endpoint='single_role')
    api.add_resource(RoleListResource, '/api/v1/roles', '/api/v1/roles/', endpoint='roles')
    
    api.add_resource(UserResource, '/api/v1/users/<user_id>', '/api/v1/users/<user_id>/', endpoint='single_user')
    api.add_resource(UserListResource, '/api/v1/users', '/api/v1/users/', endpoint='users')
    api.add_resource(UserSignupResource, '/api/v1/signup', '/api/v1/signup/', endpoint='signup_user')
    api.add_resource(UserLoginResource, '/api/v1/login', '/api/v1/login/', endpoint='login_user')
    api.add_resource(UserLogoutResource, '/api/v1/logout', '/api/v1/logout/', endpoint='logout_user')

    # handle default 404 exceptions with a custom response
    @app.errorhandler(404)
    def resource_not_found(exception):
        response = jsonify(dict(status='fail', data={
                    'error':'Not found', 'message':'The requested URL was'
                    ' not found on the server. If you entered the URL '
                    'manually please check and try again'
                }))
        response.status_code = 404
        return response

    # handle default 500 exceptions with a custom response
    @app.errorhandler(500)
    def internal_server_error(error):
        response = jsonify(dict(status=error,error='Internal Server Error',
                    message='The server encountered an internal error and was' 
                    ' unable to complete your request.  Either the server is'
                    ' overloaded or there is an error in the application'))
        response.status_code = 500
        return response

    return app

# enable flask commands
app = create_flask_app(os.getenv("FLASK_CONFIG"))
