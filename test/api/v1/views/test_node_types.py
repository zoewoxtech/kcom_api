import json
import pytest
from datetime import date

from kcom_api.test.base import BaseTestCase
from kcom_api.api.models import db, Node, NodeType


class NodeTypeTestCase(BaseTestCase):

    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()

        self.first_node_type = NodeType(
            name="page"
        )
        self.first_node_type.save()
        self.second_node_type = NodeType(
            name="blog"
        )
        self.second_node_type.save()
        self.third_node_type = NodeType(
            name="third"
        )
        self.third_node_type.save()
        self.fake_node_type = NodeType(
            name="fake"
        )
        self.fake_node_type.save()

        self.first_node_type_id = NodeType.query.filter_by(
            name='page').first().id
        self.third_node_type_id = NodeType.query.filter_by(
            name='third').first().id
    
    # Tests update one node-type
    def test_unauthorized_access_to_update_node_type(self):
        data = { 'name': 'pages' }
        response = self.client.put('api/v1/node_types/{0}'.format(self.first_node_type_id),
            data=json.dumps(data), content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    
    def test_non_super_admin_update_node_type(self):
        self.login("johndoe", "password")
        data = { 'name': 'pages' }
        response = self.client.put('api/v1/node_types/{0}'.format(self.first_node_type_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_update_request_is_valid_json(self):
        self.login("super_admin", "password")
        data = 'not JSON'
        response = self.client.put('api/v1/node_types/{0}'.format(self.first_node_type_id),
            data=json.dumps(data), content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'Request must be a valid JSON')
        self.assert400(response)

    def test_update_non_existent_node_type(self):
        self.login("super_admin", "password")
        data = { 'name': 'page' }
        response = self.client.put('api/v1/node_types/asdsd',
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'This resource does not exist')
        self.assert404(response)

    def test_update_default_node_type(self):
        self.login("super_admin", "password")
        data = { 'name': 'page' }
        response = self.client.put('api/v1/node_types/{0}'.format(self.first_node_type_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You cannot edit any of the default content types')
        self.assert401(response)

    def test_update_with_existing_node_type_name(self):
        self.login("super_admin", "password")
        data = { 'name': 'fake' }
        response = self.client.put('api/v1/node_types/{0}'.format(self.third_node_type_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'This content type already exists.')
        self.assert400(response)

    def test_update_node_type_with_no_name(self):
        self.login("super_admin", "password")
        data = { 'name': '' }
        response = self.client.put('api/v1/node_types/{0}'.format(self.third_node_type_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'The content type name is required')
        self.assert400(response)

    def test_update_node_type_successful(self):
        self.login("super_admin", "password")
        data = { 'name': 'tester' }
        response = self.client.put('api/v1/node_types/{0}'.format(self.third_node_type_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['node_type']['name'], 'tester')
        self.assert200(response)

    # Tests get one node-type
    def test_unauthorized_access_to_get_one_node_type(self):
        response = self.client.get('api/v1/node_types/wrong',
            content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    def test_non_super_admin_get_one_node_type(self):
        self.login("johndoe", "password")
        response = self.client.get('api/v1/node_types/wrong',
            content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_super_admin_get_wrong_node_type(self):
        self.login("super_admin", "password")
        response = self.client.get('api/v1/node_types/454578',
            content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'This resource does not exist')
        self.assert404(response)

    def test_get_one_node_type_successful(self):
        self.login("super_admin", "password")
        response = self.client.get('api/v1/node_types/{0}'.format(self.first_node_type_id))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assert200(response)
    
    # Tests for delete node-type
    def test_unauthorized_access_to_delete_one_node_type(self):
        response = self.client.delete('api/v1/node_types/wrong')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    def test_non_super_admin_delete_one_node_type(self):
        self.login("johndoe", "password")
        response = self.client.delete('api/v1/node_types/wrong',)
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_super_admin_delete_non_existent_node_type(self):
        self.login("super_admin", "password")
        response = self.client.delete('api/v1/node_types/454578',)
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'This resource does not exist')
        self.assert404(response)

    def test_super_admin_delete_node_type_with_nodes(self):
        self.login("super_admin", "password")
        Node(
            node_type_id=self.third_node_type_id,
            title='test',
            content='test',
            slug='test',
            meta={}
        ).save()
        response = self.client.delete('api/v1/node_types/{0}'.format(self.third_node_type_id),)
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'This content type has data and cannot be deleted')
        self.assert400(response)

    def test_super_admin_delete_default_node_type(self):
        self.login("super_admin", "password")
        response = self.client.delete('api/v1/node_types/{0}'.format(self.first_node_type_id),)
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You cannot delete a default content type')
        self.assert401(response)
    
    def test_super_admin_delete_one_node_type_successful(self):
        self.login("super_admin", "password")
        response = self.client.delete('api/v1/node_types/{0}'.format(self.third_node_type_id))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assert200(response)


class NodeTypeListTestCase(BaseTestCase):

    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()

        self.first_node_type = NodeType(
            name="events"
        )
        self.first_node_type.save()
        self.second_node_type = NodeType(
            name="blogs"
        )
        self.second_node_type.save()    
        self.first_node_type_id = NodeType.query.filter_by(
            name='events').first().id

    # Tests for creating node-types
    def test_unauthorized_access_to_create_node_type(self):
        data = { 'name': 'pages' }
        response = self.client.post('api/v1/node_types',
            data=json.dumps(data), content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    def test_non_super_admin_create_node_type(self):
        self.login("johndoe", "password")
        data = { 'name': 'pages' }
        response = self.client.post('api/v1/node_types',
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_create_request_is_valid_json(self):
        self.login("super_admin", "password")
        data = 'not JSON'
        response = self.client.post('api/v1/node_types',
            data=json.dumps(data), content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'Request must be a valid JSON')
        self.assert400(response)

    def test_create_no_name_node_type(self):
        self.login("super_admin", "password")
        data = { 'name': '' }
        response = self.client.post('api/v1/node_types',
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'The content type name is required')
        self.assert400(response)

    def test_create_duplicate_node_type(self):
        self.login("super_admin", "password")
        data = { 'name': 'events' }
        response = self.client.post('api/v1/node_types',
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'This content type already exists.')
        self.assert400(response)

    def test_create_node_type_successfully(self):
        self.login("super_admin", "password")
        data = { 'name': 'pages' }
        response = self.client.post('api/v1/node_types',
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['message'],
            'Successfully created {0} content type'.format(data['name']))
        self.assertEqual(response.status_code, 201)

    # Tests for get-all node-types
    def test_unauthorized_access_to_get_all_node_type(self):
        response = self.client.get('api/v1/node_types',
            content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    def test_non_super_admin_get_all_node_type(self):
        self.login("johndoe", "password")
        response = self.client.get('api/v1/node_types',
            content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_get_all_devotionals(self):
        self.login("super_admin", "password")
        response = self.client.get('api/v1/node_types/')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(len(response_data['data']['node_types']), 2)
        self.assert200(response)
    