import json
import pytest

from kcom_api.test.base import BaseTestCase
from kcom_api.api.models import db, User, Node, NodeType


class NodeTestCase(BaseTestCase):
    
    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()

        self.super_admin_id = User.query.filter_by(username='super_admin').first().id
        self.johndoe_id = User.query.filter_by(username='johndoe').first().id

        self.blog = NodeType(
            name="blog"
        )
        self.blog.save()
        self.page = NodeType(
            name="page"
        )
        self.page.save()
        self.blog_node_type = NodeType.query.filter_by(name='blog').first()
        self.page_node_type = NodeType.query.filter_by(name='page').first()

        self.first_node = Node(
            title='first node',
            node_type_id=self.blog_node_type.id,
            content='first node content',
            status='published',
            user_id=self.johndoe_id,
            slug='node1'
        )
        self.first_node.save()

        self.second_node = Node(
            title='second node',
            node_type_id=self.page_node_type.id,
            content='second node content',
            status='published',
            user_id=self.super_admin_id,
            slug='node2'
        )
        self.second_node.save()

        self.third_node = Node(
            title='third node',
            node_type_id=self.blog_node_type.id,
            content='third node content',
            user_id=self.johndoe_id,
            slug='node3'
        )
        self.third_node.save()

        self.fourth_node = Node(
            title='fourth node',
            node_type_id=self.blog_node_type.id,
            content='fourth node content',
            user_id=self.super_admin_id,
            slug='node4'
        )
        self.fourth_node.save()

        self.fifth_node = Node(
            title='fifth node',
            node_type_id=self.blog_node_type.id,
            content='fifth node content',
            user_id=self.super_admin_id,
            status='published',
            slug='node5'
        )
        self.fifth_node.save()

        self.first_node_id = Node.query.filter_by(title='first node').first().slug
        self.second_node_id = Node.query.filter_by(title='second node').first().slug
        self.third_node_id = Node.query.filter_by(title='third node').first().slug
        self.fourth_node_id = Node.query.filter_by(title='fourth node').first().slug
        self.fifth_node_id = Node.query.filter_by(title='fifth node').first().slug
        self.blog_node = Node.query.filter_by(title='first node', node_type_id=self.blog_node_type.id).first()
        self.page_node = Node.query.filter_by(title='second node', node_type_id=self.page_node_type.id).first()

    # tests fetch a single node without authentication
    def test_fetch_single_non_existing_node_type(self):
        response = self.client.get('api/v1/node/not_exist/{}'.format(
            self.first_node_id)
        )
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'The requested content-type does not exist')
        self.assert404(response)

    def test_fetch_single_non_existing_node(self):
        response = self.client.get('api/v1/node/{}/1000'.format(
            self.blog_node_type.name)
        )
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'This content does not exist')
        self.assert404(response)

    def test_fetch_single_page_by_non_admin(self):
        response = self.client.get('api/v1/node/{}/{}'.format(
            self.page_node_type.name, self.second_node_id)
        )
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['node']['slug'], self.second_node_id)
        self.assert200(response)

    def test_fetch_single_page_by_admin(self):
        response = self.client.get('api/v1/node/{}/{}'.format(
            self.page_node_type.name, self.second_node_id)
        )
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['node']['slug'], self.second_node_id)
        self.assert200(response)

    def test_fetch_draft_by_unauthorized_user(self):
        response = self.client.get('api/v1/node/{}/{}'.format(
            self.blog_node_type.name, self.fourth_node_id)
        )
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to view this content')
        self.assert400(response)
    
    def test_fetch_draft_by_authorized_user(self):
        self.login('johndoe', 'password')
        response = self.client.get('api/v1/node/{}/{}'.format(
            self.blog_node_type.name, self.third_node_id)
        )
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['node']['slug'], self.third_node_id)
        self.assert200(response)

    def test_fetch_draft_by_admin(self):
        self.login('admin', 'password')
        response = self.client.get('api/v1/node/{}/{}'.format(
            self.blog_node_type.name, self.third_node_id)
        )
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['node']['slug'], self.third_node_id)
        self.assert200(response)

    def test_fetch_published_node_by_authorized_user(self):
        response = self.client.get('api/v1/node/{}/{}'.format(
            self.blog_node_type.name, self.fifth_node_id)
        )
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['node']['slug'], self.fifth_node_id)

    # tests updating a single node
    def test_update_node_by_non_loggedin_user(self):
        response = self.client.put("api/v1/node/{0}/{1}".format(self.blog_node_type.name, self.blog_node.id))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'You must be logged in to view this page')
        self.assert401(response)
    
    def test_non_authorized_update_node(self):
        self.login("guest", "password")
        response = self.client.put("api/v1/node/{0}/{1}".format(self.blog_node_type.name, self.blog_node.id))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_update_request_is_valid_json(self):
        self.login("johndoe", "password")
        data = 'not JSON'
        response = self.client.put("api/v1/node/{0}/{1}".format(self.blog_node_type.name, self.blog_node.id),
            data=json.dumps(data), content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'Request must be a valid JSON')
        self.assert400(response)

    def test_update_non_existent_node_type(self):
        self.login("johndoe", "password")
        data = { "status": "draft" }
        response = self.client.put("api/v1/node/bloggertest/{0}".format(self.blog_node.id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'The requested content-type does not exist')
        self.assert404(response)

    def test_update_non_existent_node(self):
        self.login("johndoe", "password")
        data = { "status": "draft" }
        response = self.client.put("api/v1/node/{0}/123456".format(self.blog_node_type.name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            "The requested {0} does not exist".format(self.blog_node_type.name))
        self.assert404(response)

    def test_unauthorized_update_non_blog_node(self):
        self.login("janedoe", "password")
        data = { "status": "draft" }
        response = self.client.put("api/v1/node/{0}/{1}".format(self.page_node_type.name, self.page_node.slug),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            f"You are not authorized to update this {self.page_node_type.name}")
        self.assert401(response)

    def test_update_blog_with_existing_slug(self):
        self.login("johndoe", "password")
        data = { "status": "draft", "slug": "node3", "title": "node3" }
        response = self.client.put("api/v1/node/{0}/{1}".format(self.blog_node_type.name, self.blog_node.slug),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            "{0} with this title already exists".format(self.blog_node_type.name))
        self.assert400(response)

    def test_update_blog_with_invalid_field(self):
        self.login("johndoe", "password")
        data = { "status": "draft", "slug": "nodes1", "key": "value" }
        response = self.client.put("api/v1/node/{0}/{1}".format(self.blog_node_type.name, self.blog_node.slug),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            "No invalid field(s) allowed")
        self.assert400(response)

    def test_update_blog_by_blog_owner(self):
        self.login("johndoe", "password")
        data = { "status": "published" }
        response = self.client.put("api/v1/node/{0}/{1}".format(self.blog_node_type.name, self.blog_node.slug),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['message'],
            "Successfully updated post with title {0}".format(self.blog_node.title))
        self.assert200(response)

    def test_update_node_by_admin(self):
        self.login("admin", "password")
        data = { "status": "published" }
        response = self.client.put("api/v1/node/{0}/{1}".format(self.blog_node_type.name, self.blog_node.slug),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['message'],
            "Successfully updated post with title {0}".format(self.blog_node.title))
        self.assert200(response)


class NodeListTestCase(BaseTestCase):
  
    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()
        self.user_id = User.query.filter_by(username='johndoe').first().id
        self.super_admin_id = User.query.filter_by(username='super_admin').first().id
        self.johndoe_id = User.query.filter_by(username='johndoe').first().id

        self.first_node_type = NodeType(
            name="blog"
        )
        self.first_node_type.save()
        self.first_node_type_id = NodeType.query.filter_by(
            name='blog').first().id
        self.first_node_type_name = NodeType.query.filter_by(
            name='blog').first().name

        self.first_node = Node(
            title='first node',
            node_type_id=self.first_node_type_id,
            content='first node content',
            status='published',
            user_id=self.user_id,
            slug='node1'
        )
        self.first_node.save()

        self.second_node = Node(
            title='second node',
            node_type_id=self.first_node_type_id,
            content='second node content',
            status='published',
            user_id=self.user_id,
            slug='node2'
        )
        self.second_node.save()

        self.third_node = Node(
            title='third node',
            node_type_id=self.first_node_type_id,
            content='third node content',
            status='published',
            user_id=self.user_id,
            slug='node3'
        )
        self.third_node.save()



        self.blog = NodeType(
            name="blog"
        )
        self.blog.save()
        self.page = NodeType(
            name="page"
        )
        self.page.save()
        self.blog_node_type = NodeType.query.filter_by(name='blog').first()
        self.page_node_type = NodeType.query.filter_by(name='page').first()

        self.fourth_node = Node(
            title='fourth node',
            node_type_id=self.blog_node_type.id,
            content='fourth node content',
            user_id=self.super_admin_id,
            slug='node4'
        )
        self.fourth_node.save()

        self.fifth_node = Node(
            title='fifth node',
            node_type_id=self.blog_node_type.id,
            content='fifth node content',
            user_id=self.super_admin_id,
            status='published',
            slug='node5'
        )
        self.fifth_node.save()

        self.first_node_id = Node.query.filter_by(title='first node').first().slug
        self.second_node_id = Node.query.filter_by(title='second node').first().slug
        self.third_node_id = Node.query.filter_by(title='third node').first().slug
        self.fourth_node_id = Node.query.filter_by(title='fourth node').first().slug
        self.fifth_node_id = Node.query.filter_by(title='fifth node').first().slug
        self.blog_node = Node.query.filter_by(title='first node', node_type_id=self.blog_node_type.id).first()
        self.page_node = Node.query.filter_by(title='second node', node_type_id=self.first_node_type.id).first()

    # Tests to view all nodes not logged in
    def test_fetch_data_successful(self):
        response = self.client.get('api/v1/node/{}'.format(self.first_node_type_name))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(len(response_data['data']['nodes']), 3)
        self.assert200(response)

    def test_fetch_invalid_node_type(self):
        response = self.client.get('api/v1/node/page')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], "This url doesn't exist")
        self.assert404(response)

    def test_fetch_invalid_node_type(self):
        response = self.client.get('api/v1/node/not_exist')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'The not_exist node does not exist')
        self.assert400(response)
    
    # TESTS TRASH NODES
    def test_delete_node_by_non_loggedin_user(self):
        data = { 'nodes': [self.blog_node.slug] }
        response = self.client.patch("api/v1/node/{0}/".format(self.blog_node_type.name,
            data=json.dumps(data), content_type='application/json'))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'You must be logged in to view this page')
        self.assert401(response)
    
    def test_non_authorized_delete_node(self):
        self.login("guest", "password")
        data = { 'nodes': [self.blog_node.slug] }
        response = self.client.patch("api/v1/node/{0}/".format(self.blog_node_type.name,
            data=json.dumps(data), content_type='application/json'))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_unauthorized_delete_non_blog_node(self):
        self.login("janedoe", "password")
        data = { 'nodes': [self.page_node.slug] }
        response = self.client.patch("api/v1/node/{0}/".format(self.page_node_type.name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            "You are not authorized to perform this operation")
        self.assert401(response)

    def test_delete_non_existent_node(self):
        self.login("super_admin", "password")
        data = { 'nodes': [13234354] }
        response = self.client.patch("api/v1/node/{0}".format(self.page_node_type.name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['message'],
            "Some post(s) could not be found. ")
        self.assert200(response)

    def test_delete_node_successfully(self):
        self.login("super_admin", "password")
        data = { 'nodes': [self.page_node.slug], 'nodeAction': 'delete' }
        response = self.client.patch("api/v1/node/{0}".format(self.blog_node_type.name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assert200(response)
        

class NodeAccessListTestCase(BaseTestCase):
  
    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()
        self.john_user_id = User.query.filter_by(username='johndoe').first().id
        self.jane_user_id = User.query.filter_by(username='janedoe').first().id
        self.admin_id = User.query.filter_by(username='admin').first().id

        self.first_node_type = NodeType(
            name="page"
        )
        self.first_node_type.save()
        self.first_node_type_id = NodeType.query.filter_by(
            name='page').first().id
        self.first_node_type_name = NodeType.query.filter_by(
            name='page').first().name
        
        self.second_node_type = NodeType(
            name="blog"
        )
        self.second_node_type.save()
        self.second_node_type_id = NodeType.query.filter_by(
            name='blog').first().id
        self.second_node_type_name = NodeType.query.filter_by(
            name='blog').first().name

        self.first_node = Node(
            title='first node',
            node_type_id=self.first_node_type_id,
            content='first node content',
            status='published',
            user_id=self.admin_id,
            slug='node1'
        )
        self.first_node.save()

        self.second_node = Node(
            title='second node',
            node_type_id=self.second_node_type_id,
            content='second node content',
            status='published',
            user_id=self.john_user_id,
            slug='node2'
        )
        self.second_node.save()

        self.third_node = Node(
            title='third node',
            node_type_id=self.second_node_type_id,
            content='third node content',
            status='published',
            user_id=self.john_user_id,
            slug='node3'
        )
        self.third_node.save()

        self.fourth_node = Node(
            title='fourth node',
            node_type_id=self.second_node_type_id,
            content='fourth node content',
            user_id=self.john_user_id,
            slug='node4'
        )
        self.fourth_node.save()

        self.fifth_node = Node(
            title='fifth node',
            node_type_id=self.second_node_type_id,
            content='fifth node content',
            user_id=self.jane_user_id,
            slug='node5'
        )
        self.fifth_node.save()

        self.sixth_node = Node(
            title='sixth node',
            node_type_id=self.second_node_type_id,
            content='sixth node content',
            status='published',
            user_id=self.jane_user_id,
            slug='node6'
        )
        self.sixth_node.save()

    # Tests to view all nodes when logged in
    def test_unauthorized_fetch_data(self):
        response = self.client.get('api/v1/node/access/{}'.format(self.second_node_type_name))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    def test_fetch_data_by_authenticated_successful(self):
        self.login("johndoe", "password")
        response = self.client.get('api/v1/node/access/{}'.format(self.second_node_type_name))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(len(response_data['data']['nodes']), 3)
        self.assertEqual(response_data['data']['total_nodes'], 4)
        self.assert200(response)

    def test_fetch_data_by_admin_successful(self):
        self.login("admin", "password")
        response = self.client.get('api/v1/node/access/{}'.format(self.second_node_type_name))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(len(response_data['data']['nodes']), 3)
        self.assertEqual(response_data['data']['total_nodes'], 5)
        self.assert200(response)

    def test_fetch_page_by_authenticated_successful(self):
        self.login("johndoe", "password")
        response = self.client.get('api/v1/node/access/{}'.format(self.first_node_type_name))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to view this node')
        self.assert400(response)

    def test_fetch_page_by_admin_successful(self):
        self.login("admin", "password")
        response = self.client.get('api/v1/node/access/{}'.format(self.first_node_type_name))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(len(response_data['data']['nodes']), 1)
        self.assert200(response)

    def test_fetch_invalid_node_type(self):
        self.login("johndoe", "password")
        response = self.client.get('api/v1/node/access/not_exist')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'The not_exist content type does not exist')
        self.assert400(response)

    # Tests create a node
    def test_non_login_access_to_create_node_type(self):
        data = { 'title': 'pages' }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    def test_non_authorized_user_create_node_type(self):
        self.login("guest", "password")
        data = { 'name': 'pages' }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_create_request_is_valid_json(self):
        self.login("super_admin", "password")
        data = 'not JSON'
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'Request must be a valid JSON')
        self.assert400(response)

    def test_create_page_by_non_admin(self):
        self.login("johndoe", "password")
        data = { "title": "test", "slug": "test", "content": "test", "node_type_id": self.first_node_type_id, "meta": {} }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_create_node_with_no_slug(self):
        self.login("super_admin", "password")
        data = { 'title': 'test', 'content': 'test', 'meta': {} }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['message'],
            f"Successfully created post with title {data['title']}")
        self.assertEqual(response.status_code, 201)

    def test_create_node_with_no_content(self):
        self.login("super_admin", "password")
        data = { 'title': 'test', 'slug': 'test', 'content': '', 'node_type_id': 'test' }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'content is required')
        self.assert400(response)

    def test_create_node_with_no_title(self):
        self.login("super_admin", "password")
        data = { 'title': '', 'slug': 'test', 'content': 'test' }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'title is required')
        self.assert400(response)

    def test_create_node_with_invalid_meta(self):
        self.login("super_admin", "password")
        data = { 'title': 'test', 'slug': 'test', 'content': 'test', 'node_type_id': 12, 'meta': '' }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'meta must be a/an dictionary')
        self.assert400(response)
    
    def test_create_node_with_non_existent_node_type(self):
        self.login("super_admin", "password")
        data = { 'title': 'test', 'slug': 'test', 'content': 'test', 'meta': {} }
        response = self.client.post('api/v1/node/access/tester'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'The requested content type does not exist')
        self.assert404(response)

    def test_create_node_with_existing_slug(self):
        self.login("super_admin", "password")
        data = { 'title': 'test', 'slug': 'node1', 'content': 'test', 'node_type_id': self.first_node_type_name, 'meta': {} }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            "page already exists with title 'test'")
        self.assert400(response)

    def test_create_node_with_invalid_key(self):
        self.login("super_admin", "password")
        data = { 'key': 'key', 'title': 'test', 'slug': 'slug', 'content': 'test', 'node_type_id': self.first_node_type_id, 'meta': {} }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'No invalid field(s) allowed')
        self.assert400(response)

    def test_create_node_with_invalid_status(self):
        self.login("super_admin", "password")
        data = { 'status': 'key', 'title': 'test', 'content': 'test', 'meta': {} }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'Incorrect status selected')
        self.assert400(response)

    def test_create_blog_by_non_admin(self):
        self.login("johndoe", "password")
        data = { "title": "test", "content": "test", "meta": {} }
        response = self.client.post('api/v1/node/access/{}'.format(self.second_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assertEqual(response.status_code, 401)

    def test_create_node_successfully(self):
        self.login("super_admin", "password")
        data = { 'title': 'test', 'content': 'test', 'meta': {} }
        response = self.client.post('api/v1/node/access/{}'.format(self.first_node_type_name),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['message'],
            f"Successfully created post with title {data['title']}")
        self.assertEqual(response.status_code, 201)
