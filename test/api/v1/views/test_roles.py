import json
import pytest
from datetime import date

from kcom_api.test.base import BaseTestCase
from kcom_api.api.models import db, Role


class RoleTestCase(BaseTestCase):

    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()
        
        self.new_role = Role(
            title='test_role',
            description='test role'
        )
        self.new_role.save()

        self.new_role_id = Role.query.filter_by(title='test_role').first().id

    # tests delete roles
    def test_not_logged_in_delete_role(self):
        response = self.client.delete('api/v1/roles/hkjk', content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    def test_unauthorized_delete_role(self):
        self.login('johndoe', 'password')
        response = self.client.delete('api/v1/roles/afdsg', content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_delete_role_when_not_found(self):
        self.login("super_admin", "password")
        response = self.client.delete('api/v1/roles/asd123')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'This resource does not exist')
        self.assert404(response)

    def test_delete_default_super_admin_role_fail(self):
        self.login("super_admin", "password")
        super_admin_id = Role.query.filter_by(title='super_admin').first().id
        response = self.client.delete('api/v1/roles/{}'.format(super_admin_id))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'You cannot edit any of the default roles')
        self.assert400(response)

    def test_delete_role_successful(self):
        self.login("super_admin", "password")
        new_role = Role(
            title='delete_role',
            description='for testing role delete'
        )
        new_role.save()
        role_id = Role.query.filter_by(title='delete_role').first().id

        response = self.client.delete('api/v1/roles/{}'.format(role_id))
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertIsNone(response_data['data'])
        self.assert200(response)

    # tests update roles
    def test_not_logged_in_update_role(self):
        response = self.client.put('api/v1/roles/hkjk', content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    def test_unauthorized_update_role(self):
        self.login('johndoe', 'password')
        response = self.client.put('api/v1/roles/afdsg', content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_update_request_is_valid_json(self):
        self.login("super_admin", "password")
        response = self.client.put('api/v1/roles/asdsd', content_type='text/plain')
        response_data = json.loads(response.data)

        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'Request must be a valid JSON')
        self.assert400(response)

    def test_update_role_when_not_found(self):
        self.login("super_admin", "password")
        data = { 'description': "ade"}
        response = self.client.put('api/v1/roles/asd123', )
        response = self.client.put('api/v1/roles/asdsde', data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'This resource does not exist')
        self.assert404(response)

    def test_update_default_super_admin_role_fail(self):
        self.login("super_admin", "password")
        data = {'title': 'new_super_admin'}
        super_admin_id = Role.query.filter_by(title='super_admin').first().id
        response = self.client.put('api/v1/roles/{}'.format(super_admin_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'You cannot edit any of the default roles')
        self.assert400(response)

    def test_duplicate_role_title(self):
        self.login("super_admin", "password")
        data = {'title': 'super_admin'}
        response = self.client.put('api/v1/roles/{}'.format(self.new_role_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'Role with this title already exists')

    def test_update_empty_fields(self):
        self.login("super_admin", "password")
        data = {'description': ''}
        response = self.client.put('api/v1/roles/{}'.format(self.new_role_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'Fields cannot be empty')

    def test_update_invalid_fields(self):
        self.login("super_admin", "password")
        data = {'unknown_field': 'demo'}
        response = self.client.put('api/v1/roles/{}'.format(self.new_role_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'No invalid field(s) allowed')

    def test_update_successful(self):
        self.login("super_admin", "password")
        data = {'description': 'new description'}
        response = self.client.put('api/v1/roles/{}'.format(self.new_role_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['role']['id'], self.new_role_id)
        self.assertEqual(response_data['data']['role']['description'], 'new description')
        self.assert200(response)


class RoleListTestCase(BaseTestCase):

    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()
    
    # tests get all roles
    def test_not_logged_in_get_roles(self):
        response = self.client.get('api/v1/roles', content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    def test_unauthorized_get_roles(self):
        self.login('johndoe', 'password')
        response = self.client.get('api/v1/roles', content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not authorized to perform this operation')
        self.assert401(response)

    def test_get_roles_successful_by_super_admin(self):
        self.login('super_admin', 'password')
        response = self.client.get('api/v1/roles')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assert200(response)

    # tests create new role
    def test_user_not_logged_in_to_create_roles(self):
        data = "not JSON"
        response = self.client.post('api/v1/roles',
            data=json.dumps(data), content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assert401(response)

    def test_create_request_is_valid_json(self):
        self.login("super_admin", "password")
        response = self.client.post('api/v1/roles/', content_type='text/plain')
        response_data = json.loads(response.data)

        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'Request must be a valid JSON')
        self.assert400(response)

    def test_duplicate_role_title(self):
        self.login("super_admin", "password")
        duplicate_role = {
            'title': 'super_admin',
            'description': 'super_admin priviledges'
        }
        response = self.client.post('api/v1/roles', data=json.dumps(duplicate_role), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'Role already exists')

    def test_create_role_with_no_description(self):
        self.login("super_admin", "password")
        new_role = {
            'title': 'demo',
            'description': ''
        }
        response = self.client.post('api/v1/roles', data=json.dumps(new_role),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'description is required')

    def test_create_role_with_no_title(self):
        self.login("super_admin", "password")
        new_role = {
            'title': '',
            'description': 'test'
        }
        response = self.client.post('api/v1/roles', data=json.dumps(new_role),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'title is required')

    def test_create_invalid_fields_role(self):
        self.login("super_admin", "password")
        new_role = {
            'unknown_field': 'demo',
            'title': 'demo',
            'description': 'demo'
        }
        response = self.client.post('api/v1/roles', data=json.dumps(new_role),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'No invalid field(s) allowed')

    def test_unauthorized_role_creation(self):
        self.login("johndoe", "password")
        new_role = {
            'title': 'new_role',
            'description': 'new_role priviledges'
        }
        response = self.client.post('api/v1/roles', data=json.dumps(new_role),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert401(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'You are not authorized to perform this operation')

    def test_role_created_successfully(self):
        self.login("super_admin", "password")
        new_role = {
            'title': 'test_role',
            'description': 'test_role priviledges'
        }
        response = self.client.post('api/v1/roles', content_type='application/json',
            data=json.dumps(new_role))
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['role']['title'], 'test_role')
        self.assertEqual(response_data['data']['role']['description'], 'test_role priviledges')
