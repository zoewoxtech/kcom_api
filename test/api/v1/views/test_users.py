import json
import pytest

from kcom_api.test.base import BaseTestCase
from kcom_api.api.models import db, User, Role


class UserTestCase(BaseTestCase):

    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()
    
    # tests get user profile
    def test_unauthorized_to_view_profile(self):
        johndoe_id = User.query.filter_by(username='johndoe').first().id
        response = self.client.get('api/v1/users/{}'.format(johndoe_id))
        response_data = json.loads(response.data)

        self.assertEquals(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assertEquals(response_data['status'], 'fail')
        self.assert401(response)

    def test_view_non_existent_user(self):
        self.login("johndoe", "password")
        response = self.client.get('api/v1/users/asdf')
        response_data = json.loads(response.data)

        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'This resource does not exist')
        self.assert404(response)

    def test_view_user_profile_successful(self):
        self.login('johndoe', 'password')
        johndoe_id = User.query.filter_by(username='johndoe').first().id
        response = self.client.get('api/v1/users/{}'.format(johndoe_id))
        response_data = json.loads(response.data)

        self.assert_(response_data['data']['user'])
        self.assertEquals(response_data['status'], 'success')
        self.assert200(response)

    # tests update user profile
    def test_unauthorized_to_update_profile(self):
        johndoe_id = User.query.filter_by(username='johndoe').first().id
        response = self.client.put('api/v1/users/{}'.format(johndoe_id))
        response_data = json.loads(response.data)

        self.assertEquals(response_data['data']['message'],
            'You must be logged in to view this page')
        self.assertEquals(response_data['status'], 'fail')
        self.assert401(response)

    def test_update_request_is_valid_json(self):
        self.login("johndoe", "password")
        johndoe_id = User.query.filter_by(username='johndoe').first().id

        response = self.client.put('api/v1/users/{}'.format(johndoe_id),
            content_type='text/plain')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'Request must be a valid JSON')
        self.assert400(response)

    def test_update_by_non_account_holder(self):
        self.login("janedoe", "password")
        johndoe_id = User.query.filter_by(username='johndoe').first().id
        data = {"bio": "johndoe's bio"}

        response = self.client.put('api/v1/users/{}'.format(johndoe_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not permitted to perform this operation')
        self.assert401(response)

    def test_invalid_update_fields(self):
        self.login("johndoe", "password")
        johndoe_id = User.query.filter_by(username='johndoe').first().id
        data = {"blah": "johndoe's bio"}

        response = self.client.put('api/v1/users/{}'.format(johndoe_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'No invalid field(s) allowed')

    def test_update_fields_are_not_empty(self):
        self.login("johndoe", "password")
        johndoe_id = User.query.filter_by(username='johndoe').first().id
        data = {'firstname': ''}
        response = self.client.put('api/v1/users/{}'.format(johndoe_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'Fields cannot be empty')
        self.assert400(response)

    def test_update_role_by_super_admin(self):
        self.login("super_admin", "password")
        super_admin_id = User.query.filter_by(username='super_admin').first().id
        admin_id = Role.query.filter_by(title='admin').first().id
        data = {'role_id': admin_id}
        response = self.client.put('api/v1/users/{}'.format(super_admin_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'],
            'You are not allowed to update your role')
        self.assert400(response)

    def test_update_by_successful_admin(self):
        self.login("admin", "password")
        johndoe_id = User.query.filter_by(username='johndoe').first().id
        data = {"bio": "johndoe's bio", 'firstname': 'johns', 'lastname': 'does'}
        response = self.client.put('api/v1/users/{}'.format(johndoe_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['user']['id'], johndoe_id)
        self.assertEqual(response_data['data']['user']['bio'], "johndoe's bio")
        self.assertEqual(response_data['data']['user']['firstname'], 'johns')
        self.assertEqual(response_data['data']['user']['lastname'], 'does')
        self.assert200(response)

    def test_update_by_successful_super_admin(self):
        self.login("super_admin", "password")
        johndoe_id = User.query.filter_by(username='johndoe').first().id
        data = {"bio": "johndoe's bio", 'firstname': 'johns', 'lastname': 'does'}
        response = self.client.put('api/v1/users/{}'.format(johndoe_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['user']['id'], johndoe_id)
        self.assertEqual(response_data['data']['user']['bio'], "johndoe's bio")
        self.assertEqual(response_data['data']['user']['firstname'], 'johns')
        self.assertEqual(response_data['data']['user']['lastname'], 'does')
        self.assert200(response)

    def test_update_user_profile_successful(self):
        self.login("johndoe", "password")
        johndoe_id = User.query.filter_by(username='johndoe').first().id
        data = {"bio": "johndoe's bio", 'firstname': 'johns', 'lastname': 'does'}
        response = self.client.put('api/v1/users/{}'.format(johndoe_id),
            data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['user']['id'], johndoe_id)
        self.assertEqual(response_data['data']['user']['bio'], "johndoe's bio")
        self.assertEqual(response_data['data']['user']['firstname'], 'johns')
        self.assertEqual(response_data['data']['user']['lastname'], 'does')
        self.assert200(response)


class UserListTestCase(BaseTestCase):

    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()
    
    # tests get all users
    def test_unauthorized_to_view_all_users(self):
        response = self.client.get('api/v1/users/')
        response_data = json.loads(response.data)

        self.assertEquals(response_data['data']['message'], 'You must be logged in to view this page')
        self.assertEquals(response_data['status'], 'fail')
        self.assert401(response)

    def test_fetch_users_fail_by_non_admin(self):
        self.login('johndoe', 'password')
        response = self.client.get('api/v1/users')
        response_data = json.loads(response.data)

        self.assertEquals(response_data['data']['message'], 'You are not authorized to perform this operation')
        self.assertEquals(response_data['status'], 'fail')
        self.assert401(response)

    def test_fetch_users_successful_by_admin(self):
        self.login('admin', 'password')
        response = self.client.get('api/v1/users')
        response_data = json.loads(response.data)

        self.assertEqual(response_data['status'], 'success')
        self.assert_(response_data['data']['users'])
        self.assert200(response)


class UserSignupTestCase(BaseTestCase):

    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()

    # tests user signup
    def test_create_request_is_valid_json(self):
        response = self.client.post('api/v1/signup/', content_type='text/plain')
        response_data = json.loads(response.data)

        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'Request must be a valid JSON')
        self.assert400(response)

    def test_user_data_already_taken(self):
        duplicate_user = {
            'title': 'Mr',
            'firstname': 'Tami',
            'lastname': 'Lister',
            'middlename': '',
            'username': 'johndoe',
            'email': 'john@doe.com',
            'password': 'password',
            'confirmPassword': 'password',
            'bio': 'Tami bio',
            'dob': '1994-04-15'
        }
        response = self.client.post('api/v1/signup', data=json.dumps(duplicate_user), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'User already exists')

    def test_signup_with_no_firstname(self):
        new_user = {
            'title': 'Miss',
            'firstname': '',
            'lastname': 'test',
            'middlename': 'a',
            'username': 'test',
            'email': 'test',
            'password': 'password',
            'confirmPassword': 'password',
            'bio': 'Tami bio',
            'dob': '1994-04-15'
        }
        response = self.client.post('api/v1/signup', data=json.dumps(new_user),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'firstname is required')

    def test_signup_with_no_lastname(self):
        new_user = {
            'title': 'Miss',
            'firstname': 'test',
            'lastname': '',
            'middlename': 'a',
            'username': 'test',
            'email': 'test',
            'password': 'password',
            'confirmPassword': 'password',
            'bio': 'Tami bio',
            'dob': '1994-04-15'
        }
        response = self.client.post('api/v1/signup', data=json.dumps(new_user),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'lastname is required')

    def test_signup_with_no_username(self):
        new_user = {
            'title': 'Miss',
            'firstname': 'test',
            'lastname': 'test',
            'middlename': 'a',
            'username': '',
            'email': 'test',
            'password': 'password',
            'confirmPassword': 'password',
            'bio': 'Tami bio',
            'dob': '1994-04-15'
        }
        response = self.client.post('api/v1/signup', data=json.dumps(new_user),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'username is required')

    def test_signup_with_no_email(self):
        new_user = {
            'title': 'Miss',
            'firstname': 'test',
            'lastname': 'test',
            'middlename': 'a',
            'username': 'test',
            'email': '',
            'password': 'password',
            'confirmPassword': 'password',
            'bio': 'Tami bio',
            'dob': '1994-04-15'
        }
        response = self.client.post('api/v1/signup', data=json.dumps(new_user),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'email is required')

    def test_signup_with_no_password(self):
        new_user = {
            'title': 'Miss',
            'firstname': 'test',
            'lastname': 'test',
            'middlename': 'a',
            'username': 'test',
            'email': 'test',
            'password': '',
            'confirmPassword': '',
            'bio': 'Tami bio',
            'dob': '1994-04-15'
        }
        response = self.client.post('api/v1/signup', data=json.dumps(new_user),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'password is required')

    def test_signup_with_no_dob(self):
        new_user = {
            'title': 'Miss',
            'firstname': 'test',
            'lastname': 'test',
            'middlename': 'a',
            'username': 'test',
            'email': 'test',
            'password': 'password',
            'confirmPassword': 'password',
            'bio': 'Tami bio',
            'dob': ''
        }
        response = self.client.post('api/v1/signup', data=json.dumps(new_user),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'dob is required')

    def test_invalid_signup_fields(self):
        new_user = {
            'unknown_field': 'Miss',
            'title': 'Miss',
            'firstname': 'test',
            'lastname': 'test',
            'middlename': 'a',
            'username': 'test',
            'email': 'test',
            'password': 'password',
            'confirmPassword': 'password',
            'bio': 'Tami bio',
            'dob': '1994-04-15'
        }
        response = self.client.post('api/v1/signup', data=json.dumps(new_user),
                    content_type='application/json')
        response_data = json.loads(response.data)
        self.assert400(response)
        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'No invalid field(s) allowed')

    def test_user_profile_created_successfully(self):
        new_user = {
            'title': 'Miss',
            'firstname': 'Tami',
            'lastname': 'Lister',
            'middlename': 'a',
            'username': 'tami',
            'email': 'tami@lister.com',
            'password': 'password',
            'confirmPassword': 'password',
            'bio': 'Tami bio',
            'dob': '1994-04-15'
        }
        response = self.client.post('api/v1/signup', content_type='application/json',
            data=json.dumps(new_user))
        response_data = json.loads(response.data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response_data['status'], 'success')
        self.assertEqual(response_data['data']['user']['username'], 'tami')


class UserLoginTestCase(BaseTestCase):

    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()

    # tests user login
    def test_login_request_is_valid_json(self):
        response = self.client.post('api/v1/login/', content_type='text/plain')
        response_data = json.loads(response.data)

        self.assertEqual(response_data['status'], 'fail')
        self.assertEqual(response_data['data']['message'], 'Request must be a valid JSON')
        self.assert400(response)

    def test_login_with_wrong_username(self):
        data = { 'userId': 'fake_user', 'password': 'password' }
        response = self.client.post('api/v1/login', data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEquals(response_data['data']['message'], 'User does not exist')
        self.assertEquals(response_data['status'], 'fail')
        self.assert404(response)

    def test_login_with_wrong_userId_type(self):
        data = { 'userId': 123, 'password': 'wrong_password' }
        response = self.client.post('api/v1/login', data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEquals(response_data['data']['message'],
            'userId must be a/an string')
        self.assertEquals(response_data['status'], 'fail')

    def test_login_with_wrong_password(self):
        data = { 'userId': 'johndoe', 'password': 'wrong_password' }
        response = self.client.post('api/v1/login', data=json.dumps(data), content_type='application/json')
        response_data = json.loads(response.data)
        self.assertEquals(response_data['data']['message'], 'Your password is incorrect, please try again')
        self.assertEquals(response_data['status'], 'fail')
        self.assert401(response)

    def test_login_successful(self):
        data = { 'userId': 'johndoe', 'password': 'password' }
        response = self.client.post('api/v1/login', data=json.dumps(data), content_type='application/json')

        response_data = json.loads(response.data)
        self.assertEquals(response_data['data']['message'], 'Login successful')
        self.assertEquals(response_data['status'], 'success')
        self.assert_(response_data['data']['token'])
        self.assert200(response)


class UserLogoutTestCase(BaseTestCase):

    def setUp(self):
        db.drop_all()
        db.create_all()

        self.create_default_users()

    # tests user logout
    def test_user_logout(self):
        self.login('johndoe', 'password')
        response = self.client.get('api/v1/logout')
        response_data = json.loads(response.data)

        self.assertEqual(response_data['status'], 'success')
        self.assertEquals(response_data['data']['message'], 'You have been successfully logged out')
        self.assert200(response)

    def test_user_logout_failure(self):
        response = self.client.get('api/v1/logout')
        response_data = json.loads(response.data)

        self.assertEqual(response_data['status'], 'fail')
        self.assertEquals(response_data['data']['message'], 'Only logged in users can log out')
        self.assert400(response)
