import os
import json

from flask_testing import TestCase
from flask_login import LoginManager

from kcom_api.api.helper import (
    kcom_errors, create_default_user
)
from kcom_api.api.default_data import create_default_roles
from kcom_api.main import create_flask_app
from kcom_api.api.models import db, User, Role


class BaseTestCase(TestCase):

    def create_app(self):
        self.app = create_flask_app('testing')
        self.app.secret_key = os.getenv("APP_SECRET")
        login_manager = LoginManager()
        login_manager.init_app(self.app)
        # Set up user_loader
        @login_manager.user_loader
        def load_user(user_id):
            return User.query.get(user_id)
        @login_manager.unauthorized_handler
        def unauthorized_callback():
            return kcom_errors("You must be logged in to view this page", 401)
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.app.secret_key = os.getenv('APP_SECRET')
        login_manager = LoginManager()
        login_manager.init_app(self.app)
        # Set up user_loader
        @login_manager.user_loader
        def load_user(user_id):
            return User.query.get(user_id)
        @login_manager.unauthorized_handler
        def unauthorized_callback():
            return kcom_errors("You must be logged in to view this page", 401)
        self.client = self.app.test_client()

        return self.app

    def setUp(self):
        db.drop_all()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def login(self, username, password):
        return self.client.post('api/v1/login',
                data=json.dumps({
                    "userId": username,
                    "password": password
                }),
                content_type='application/json')

    def logout(self):
        return self.client.get('api/v1/logout')

    def create_default_users(self):
        create_default_roles()

        super_admin_id = Role.query.filter_by(title="super_admin").first().id
        admin_id = Role.query.filter_by(title="admin").first().id
        authenticated_id = Role.query.filter_by(title="authenticated").first().id
        guest_id = Role.query.filter_by(title="guest").first().id

        create_default_user(super_admin_id, admin_id, authenticated_id, guest_id)
    
    def create_blog(self):
        return self.client.post('api/v1/blogs',
            data=json.dumps({
                'title':'lorem',
                'body':'lorem ipsum',
                'slug':'lorem'
            }), content_type='application/json')
